package com.example.demo2springb1007.entity;

import com.alibaba.fastjson.JSONArray;

import java.util.ArrayList;

public class Back {
    private int status;
    private String msg;
    private JSONArray data;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public JSONArray getData() {
        return data;
    }

    public void setData(JSONArray data) {
        this.data = data;
    }
}
