package com.example.demo2springb1007.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.example.demo2springb1007.entity.Back;
import org.springframework.http.*;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;

@Controller
@ResponseBody
public class hello {

    Back back = new Back();

    // 为了配合微信服务器的验证信息
    @GetMapping("/")
    public String home(@RequestParam(value = "signature", defaultValue = "1") String signature,
            @RequestParam(value = "timestamp", defaultValue = "2") String timestamp,
            @RequestParam(value = "nonce", defaultValue = "3") String nonce,
            @RequestParam(value = "echostr", defaultValue = "4") String echostr) {
        System.out.println("进来--");
        System.out.println("signature:" + signature);
        System.out.println("timestamp:" + timestamp);
        System.out.println("nonce:" + nonce);
        System.out.println("echostr:" + echostr);

        return echostr;
    }

    // 根据前端传来的code，获取用户信息
    @GetMapping("/wx")
    public Object wx(@RequestParam(value = "code") String code) {
        System.out.println("code:" + code);
        RestTemplate restTemplate = new RestTemplate();
        HashMap<String, String> config = new HashMap<>();

        // 如果你开放平台登录：就借用别人公司的www.txjava.cn的账号
        config.put("APPID", "wx7287a60bb700fd21");
        config.put("SECRET", "1ef8755f92bebae8ad7bab432ba29cbf"); // 2023.03.12 change one

        // 如果你用公众号开发，就用这个配置：现在用我自己的公众号，改成你的
        // config.put("APPID","wx3680aee5b396cc0f");
        // config.put("SECRET","626c988a2e4af8cc459424187951792e");

        config.put("code", code);
        System.out.println("config.get(\"APPID\")" + config.get("APPID"));
        restTemplate.getMessageConverters().set(1, new StringHttpMessageConverter(StandardCharsets.UTF_8));

        // 1.根据前端，或者微信服务器返回来的code，去请求access_token
        String uri = "https://api.weixin.qq.com/sns/oauth2/access_token?appid="
                + config.get("APPID") + "&secret=" + config.get("SECRET") + "&code=" + config.get("code")
                + "&grant_type=authorization_code";
        System.out.println("uri:" + uri);
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.parseMediaType("application/json;charset=UTF-8"));
        HttpEntity<String> entity = new HttpEntity<String>(headers);
        ResponseEntity<String> response = restTemplate.exchange(uri, HttpMethod.GET, entity, String.class);
        System.out.println(response);
        System.out.println(response.getBody());
        JSONObject obj = JSON.parseObject(response.getBody());

        // 2.根据access_token，请求个人信息
        String uri2 = "https://api.weixin.qq.com/sns/userinfo?access_token="
                + obj.get("access_token") + "&openid=" + obj.get("openid") + "&lang=zh_CN";
        ResponseEntity<String> response2 = restTemplate.exchange(uri2, HttpMethod.GET, entity, String.class);
        System.out.println(response2);
        JSONObject obj2 = JSON.parseObject(response2.getBody());
        return obj2;
    }
}
