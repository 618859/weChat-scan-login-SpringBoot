package com.example.demo2springb1007;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Demo2SpringB1007Application {

    public static void main(String[] args) {
        SpringApplication.run(Demo2SpringB1007Application.class, args);
    }

}
