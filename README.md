# 微信扫码登录SpringBoot版本，前后端分离

## 项目1：
> 前端代码front-end:https://gitee.com/618859/wx_login_qrcode_vue.git  
> 因为是前后端分离项目，所以前后端先后启动，用微信扫码，即可看到效果  
> 自己也可以改造成自由后台项目  
> 有不懂可以微信咨询我号yizheng369

### 外观 look
![效果](look/look1.jpg)


---
## 项目2
> 另外一个项目：微信公众号开发：获取用户openid，前后端分离
> 微信公众号开发，前后端分离，前端vue2配合后台获取用户openid
## code源码
前台：https://gitee.com/618859/wechat-official-account-openid-vue2  

后台：https://gitee.com/618859/weChat-scan-login-SpringBoot

### 外观2 look
![效果](look/look2.jpg)




